Library App v 1.0
=====================
##### Author: Sławomir Lasek

#### General 
This app is made for Library management. If you run your own company which lends books, you'll be able to manage all of the books. This means adding, lending, removing books and adding lenders.
 
#### Technical requirements
- Java IDE (Eclipse for example. To download the newest version, please visit [Eclipse download page](https://www.eclipse.org/downloads/))
- Java 1.8 JRE ([Oracle download page](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)) 

#### How to run the App
Go to Main package -> LibraryApp.java Class -> RMB "Run As Java Application"
The app simulate how the library woks, eg.:
- Creating an object 'manager', class which methods are responsible for managing the library
- Creating an object 'lender', a sample person 'John Doe' 
- Adding a lender to the list of lenders
- Creating two objects 'book' and 'book1' to be added to the library
- Showing how many books are available to be lent
- Lending a book to lender 'John Doe'
- A try of removing a lent book from the library
- Searching books by title, author, author and year of publication
- Removing the book from library

#### How to run JUnit tests
JUnit5 library is required. There are two ways to set the library:
- doing necessary imports in the Test.class or
- Project -> Build Path -> Configure Build Path -> Libraries -> Add Library -> JUnit 
 
Go to Test package -> Test.java Class -> RMB "Run As JUnit Test"
Tests Manager.Class methods

#### Documentation
Java Docs (generated)  -> doc