package pl.slaweklasek.library.test;

import static org.junit.Assert.assertEquals;

import pl.slaweklasek.library.management.Manager;
import pl.slaweklasek.library.model.Book;
import pl.slaweklasek.library.model.Lender;

/**
 * This class contains JUnit tests
 * 
 * @author Sławomir Lasek
 *
 */
public class Test {
	
	@org.junit.Test
	public void testLenderList() {
		Manager manager = new Manager();
		
		assertEquals(0, manager.lenders().size());
	}

	
	@org.junit.Test
	public void testBookList() {
		Manager manager = new Manager();
		
		assertEquals(0, manager.booksList().size());
	}
	
	
	@org.junit.Test
	public void testAddingBook() {
		Manager manager = new Manager();
		Book 	book 	= new Book("Noce", 2001, "Olgierd Oxy");
		
		manager.addBook(book);
		
		assertEquals(false, manager.booksList().isEmpty());
	}
	
	
	@org.junit.Test
	public void testRemovingBook() {
		Manager manager = new Manager();
		Book 	book 	= new Book("Noce", 2001, "Olgierd Oxy");

		manager.addBook(book);
		manager.removeBook(book.getBookId());
		
		assertEquals(true, manager.booksList().isEmpty());
	}
	
	
	@org.junit.Test
	public void testBooksAvailability() {
		Manager manager = new Manager();
		Book 	book 	= new Book("Noce", 2001, "Olgierd Oxy");
		Book 	book1 	= new Book("Dnie", 2018, "Olgierd Oxy");
		
		manager.addBook(book);
		manager.addBook(book1);
		
		assertEquals(2, manager.availableBooks());
	}

	
	@org.junit.Test
	public void testSearchingBooks() {
		Manager manager = new Manager();
		Book 	book 	= new Book("Noce", 2001, "Olgierd Oxy");
		Book 	book1 	= new Book("Dnie", 2018, "Olgierd Oxy");
		
		manager.addBook(book);
		manager.addBook(book1);
		
		assertEquals(1, manager.searchBooks("Noce", "Olgierd Oxy", 2001).size());
	}
	
	
	@org.junit.Test
	public void testLendBook() {
		Manager manager = new Manager();
		Book 	book 	= new Book("Noce", 2001, "Olgierd Oxy");
		Book 	book1 	= new Book("Dnie", 2018, "Olgierd Oxy");
		Lender 	lender 	= new Lender("Mario Bro");
		
		manager.addBook(book);
		manager.addBook(book1);
		
		manager.addLender(lender);
		manager.lendBook(book.getBookId(), "Mario Bro");
		
		assertEquals("Mario Bro", book.getLender().getName());
	}
}