package pl.slaweklasek.library.model;

import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is model Class for a lender object. Contains getters and setters and toString() method
 * 
 * @author Sławomir Lasek
 *
 */
public class Lender {
	private UID 		lenderId;
	private String 		name;
	private List<Book> 	books;
	
	
	public Lender(String name) {
		this.name 		= name;
		this.lenderId 	= new UID();
		this.books		= new ArrayList<>();
	}
	
	
	public UID getLenderId() {
		return lenderId;
	}
	
	
	public void setLenderId(UID lenderId) {
		this.lenderId = lenderId;
	}
	
	
	public String getName() {
		return name;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public List<Book> getBooks() {
		return books;
	}
	
	
	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
	
	@Override
	public String toString() {
		return 	"Lender's Id: " 	+ lenderId 	+ ",\n"				+
				"Lender's name: " 	+ name 		+ ",\n"	 			+ 
				name 				+ " has lent following books: " + books;
	}
}
