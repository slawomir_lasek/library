package pl.slaweklasek.library.model;

import java.rmi.server.UID;

/**
 * This class is model Class for a book object. Contains getters and setters and toString() method
 * 
 * @author Sławomir Lasek
 *
 */
public class Book {
	private UID		bookId; 
	private String	title;
	private int		year;
	private String	author;
	private Lender	lender;

		
	public Book(String title, int year, String author) {
		this.bookId	= new UID();
		this.title	= title;
		this.year	= year;
		this.author	= author;
	}


	public UID getBookId() {
		return bookId;
	}


	public void setBookId(UID bookId) {
		this.bookId = bookId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public Lender getLender() {
		return lender;
	}


	public void setLender(Lender lender) {
		this.lender = lender;
	}


	@Override
	public String toString() {
		return 	"Book id: " 			+ bookId 	+ ",\n" 	+
				"Book title: " 			+ title 	+ ",\n" 	+
				"Year of publication: " + year 		+ ",\n" 	+
				"Author: " 				+ author 	+ ",\n" 	+
				"Lender's name: " 		+ (lender == null ? "none" : lender.getName()) + "\n\n";
	}
}