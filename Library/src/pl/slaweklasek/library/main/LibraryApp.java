package pl.slaweklasek.library.main;

import pl.slaweklasek.library.management.Manager;
import pl.slaweklasek.library.model.Book;
import pl.slaweklasek.library.model.Lender;

/**
 * Main class where the App runs 
 * 
 * @author Sławomir Lasek
 * @version 1.0
 * @since 	2018-12-26
 *
 */
public class LibraryApp {

	public static void main(String[] args) {
		
		// Creating an object 'manager', class which methods are responsible for managing the library
		Manager manager = new Manager();
		
		/* 
		 * Creating an object 'lender', a sample person 'John Doe' 
		 * The constructor creates the class object with empty array of lent books. It creates an unique id as well.
		 */ 
		Lender lender = new Lender("John Doe");
		
		// Adding a lender to the list of lenders
		manager.addLender(lender);
		
		/* 
		 * Creating two objects 'book' and 'book1' to be added to the library
		 * The constructor creates an unique id as well
		 */
		Book book 	= new Book("W pustyni i w puszczy", 2001, "Henryk Sienkiewicz");
		Book book1	= new Book("Potop", 2018, "Henryk Sienkiewicz");
		
		manager.addBook(book);
		manager.addBook(book1);	
		
		System.out.println("In our library, there are books: \n" + manager.booksList() + "\n");
		
		// Showing how many books are available to be lent
		System.out.println("The number of available books is: " + manager.availableBooks() + "\n");
		
		// Lending a book to lender 'John Doe'
		manager.lendBook(book.getBookId(), lender.getName());
		
		System.out.println("All details about lent book and the lender: \n" + book.getLender() + "\n");
		System.out.println("The number of available books is: " + manager.availableBooks() + "\n");
		
		// A try of removing a lent book from the library
		manager.removeBook(book.getBookId());
		
		System.out.println(manager.booksList());
		
		// Searching books by title, author, author and year of publication
		System.out.println("Searching a book by a title 'W pustyni i w puszczy': \n" + manager.searchBooks("W pustyni i w puszczy", null, 0));
		System.out.println("Searching a book by an author 'Henryk Sienkiewicz': \n" + manager.searchBooks(null, "Henryk Sienkiewicz", 0));
		System.out.println("Searching a book by an author 'Henryk Sienkiewicz' and year of publication '2001': \n" + manager.searchBooks(null, "Henryk Sienkiewicz", 2001));
		
		// Removing the book from library
		System.out.println("Removing a book by id: " + book1.getBookId()); 
		
		manager.removeBook(book1.getBookId());
		
		System.out.println("In our library, there are books: \n" + manager.booksList());
	}
}